#include <iostream>
#include <limits>
using namespace std;
#include <vector> 


struct ListNode{

int val;
ListNode* next;
ListNode(int x): val(x), next(NULL) {}
};

class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        
        ListNode* out = NULL;
        //for all the lists find the minimum 
        auto i = lists.begin();
        auto j =i;
        int myMin = numeric_limits<int>::max(); 
        bool foundMin = false;
        for(; i!=lists.end();i++){
           if(*i!=NULL){
   //             cout << "printing elements::" << to_string(i-lists.begin()) <<"::"<<(*i)->val << endl;
                if( (*i)->val<= myMin)
                {
                    myMin = (*i)->val;
                    foundMin = true;
                    j=i;
                }
           }
        }
 //cout <<"foundMin="<<to_string(foundMin) <<"min=" << myMin << endl;   
        if(foundMin){
              
                    *j = (*j)->next;
              out = new ListNode(myMin);
              out->next = mergeKLists(lists);
        }

      return out;
    }
};



int main(){

ListNode tmp(1);
ListNode tmp2(2);
ListNode tmptmp(3); 
tmp.next = &tmptmp; 
vector<ListNode*> v; 
v.push_back(&tmp); 
v.push_back(&tmp2); 

Solution s;
ListNode* out = s.mergeKLists(v  );
cout <<"printing out"<<endl;
while(out != NULL)
 { 
   cout << to_string(out->val) << endl;
   out = out->next;
 }
return 0;
}
