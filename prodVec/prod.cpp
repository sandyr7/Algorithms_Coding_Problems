#include <iostream>
#include <vector>
#include <iterator>
using namespace std;
#include <chrono>
typedef chrono::high_resolution_clock Clock; 
#include <string>
#define BRUTE_FORCE 0

class Solution {
public:
   // vector<int> productExceptSelf(vector<int>& nums) {
  /*   
        vector<int> out;  
        
        if(nums.size()==0)
             return out;
        
        
        //first, find the total prod
        long long  totalProd = 1;
        vector<int>::iterator i; 
        
        int zeroCnt=0;
        auto zeroLoc =nums.end();
        
        for(i=nums.begin(); i != nums.end(); i++)
        {
            if(*i!=0)
             totalProd *= *i; 
            else
            {
                zeroCnt++;
                zeroLoc = i;
            }  
                 
        }
        
        
        if(zeroCnt>0)
        {
            //all zero 
            for(i=nums.begin(); i != nums.end(); i++){
                
                if(zeroCnt==1 && zeroLoc==i)
                    out.push_back(totalProd);
                else
                    out.push_back(0);
            }
            
            return out;
        } 
       
       // brute force 
       if(BRUTE_FORCE)
       { 
        for(auto i=nums.begin(); i!= nums.end(); i++){    
         for(long long j =abs(totalProd);j>=0;  j--){
            
             if( totalProd == *i*j)
               out.push_back(j);
             if(totalProd!=0 && -totalProd == *i*j)
               out.push_back(-j);
        }    
        }
       }
        else
       {
   

*/


vector<int> productExceptSelf(vector<int>& nums) {
     
        vector<int> out(nums.size(),1);  
        vector<int>::iterator l,r; 
        long long leftMult, rightMult; 
        
        leftMult = 1;
        rightMult = 1;
        r = nums.end()-1;
        for(l=nums.begin(); l != nums.end()-1; l++)
        {
            
             leftMult *= *l;
             rightMult *= *r;
             
             out[l-nums.begin()+1] *= leftMult; 
             out[r-nums.begin()-1] *= rightMult; 
             r--;
              
        }
        
                return out;
    }


/*

(auto i=nums.begin(); i!= nums.end(); i++){    
         int j = myDiv( abs(totalProd), *i); 
         
         if((totalProd >0 && *i>0) ||(totalProd <0 && *i<0)) 
          out.push_back(j); 
         else
          out.push_back(-j);
        }
       
       }
        return out;
    }


 int myDiv( long long prod, int x){
  int y; 
  
  //first make a LUT 
  vector<long long> lut;
  for(int k=1; k<10;k++)
   lut.push_back(x*k);

   string prodStr = to_string(prod);
   string xStr = to_string(x); 

   string::iterator i= prodStr.begin();
   string tmp; 
   while( i != prodStr.end()){
   
    //extract digits 
    for(int j=0; j<=xStr.size()/sizeof(char);j++)
     { 
      tmp.push_back(*i);
      i++; 
     }

     

   
   }
 return y; 

}
*/


};


int main(){

 Solution s; 
 vector<int> a =  {1,2,3,4,5,6,7,8};//same as an array

cout <<"printing orig" << endl; 
vector<int>::iterator i;
for(i=a.begin(); i!= a.end(); i++){
 cout << *i << "\t";
}
 cout << endl; 
auto t1 = Clock::now(); 
vector<int> b =  s.productExceptSelf(a); 
auto t2 = Clock::now();



cout <<"printing prod" << endl; 

 
for(i=b.begin(); i!= b.end(); i++){
 cout << *i << "\t";
}

cout << endl; 
cout << "Product took " << chrono::duration_cast<chrono::microseconds>(t2-t1).count() << " us" << endl;
}


