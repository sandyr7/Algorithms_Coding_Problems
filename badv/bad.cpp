#include <iostream>
using namespace std; 
#include <string>
bool isBadVersion(int x);
int badVersionBin(int left, int right);
int firstBadVersion(int n);

static int stat = 0; 

int main(){

cout << "first bad version =" << to_string(firstBadVersion(2126753390)) << endl; 

}


bool isBadVersion(int x){
//2126753390 versions
//1702766719 is the first bad version.

if(x<1702766719)
  return false;
else
  return true;

}

int firstBadVersion(int n) {
        //edge 
        
        if(n<=0)
            return 0;
        
        if(n==1)
        { 
            if(isBadVersion(n))
             return n;
            else
             return 0;
        }
        
        
        // n >= 2
        //main
         return badVersionBin(1,n);
        
    }
    
    int badVersionBin( int left , int right){
        

        bool l = isBadVersion(left);
        //bool r = isBadVersion(right);          
        
        if(l==1)
            return left; 
  
        if(right-left ==1) // size(2)
         return right; //because l is not bad
        
        // we can now create a mid;
        int mid =int(  double(left)/2.0 + double(right)/2.0); 
        bool m = isBadVersion(mid);
    
        if(stat <100)
         {
         cout <<"inside::left=" << to_string(left) <<"right=" <<to_string(right) <<  "mid=" << to_string(mid) <<endl;
         stat++;
         }
        if(m==1)
            return badVersionBin(left,mid);
        else
            return badVersionBin(mid,right);
        
    }
