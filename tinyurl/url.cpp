#include <iostream>
#include <unordered_map>
#include <string>
#include <iterator>
using namespace std;


class Solution {

    protected: 
     static int urlCnt;
     static string charSet;
     static unordered_map<string, string> myMap, myMapRev; 
    public:
    
    // Encodes a URL to a shortened URL.
    string encode(string longUrl) {
     
        auto found = myMapRev.find(longUrl);
        if( found != myMapRev.end())
          return found->second;

        urlCnt++;
        string shortUrl =  encodeCnt(urlCnt);
        myMap[shortUrl] = longUrl;
        return shortUrl; 
        
    }

    
    string encodeCnt( int urlCnt){
        
        // build the char set 
        if(charSet.size()==0)
        {
            
          for(char i= 'a'; i<='z'; i++)
            charSet.push_back(i);
          for(char i= 'A'; i<='Z'; i++)
            charSet.push_back(i);  
          for(char i= '0'; i<='9'; i++)
            charSet.push_back(i);
        
        }
        //base 62 encode
        int q = urlCnt;//q >1!!
        string out="http://tinyurl.com/"; 
            
        while(q >0){
            
         int r = q%62;
         out.push_back(charSet[r]);   
         q = (q -r)/62;        
        }
           
        return out;
            
    }
    
    // Decodes a shortened URL to its original URL.
    string decode(string shortUrl) {
        
        auto longUrl = myMap.find(shortUrl);
        if(longUrl == myMap.end())
            return "Not found";
        else
            return longUrl->second;
    }
};

int Solution::urlCnt = 0;
string Solution::charSet = "";
unordered_map<string, string> Solution::myMap; //({{"http://tinyurl.com/_","https://leetcode.com/"}});
unordered_map<string, string> Solution::myMapRev; //({{"http://tinyurl.com/_","https://leetcode.com/"}});



int main(){

 Solution s; 
 cout << s.encode("https://leetcode.com/problems") << endl;
 cout << s.decode(s.encode("https://leetcode.com/problems")) << endl;
 cout << s.decode(s.encode("https://leetcode.com/problems")) << endl;

   
 cout << s.decode(s.encode("https://leetcode.com/problems1")) << endl;
 cout << s.decode(s.encode("https://leetcode.com/problems2")) << endl;

}



