#include <iostream>
using namespace std;

//tion for singly-linked list.
  struct ListNode {
      
      
      int val;
      ListNode *next;
      
      
      ListNode(int x) : val(x), next(NULL) {}
      
  };
 //
class Solution {
public:
   
  
      static ListNode* add(ListNode* listNode, ListNode* newNode){
       
       newNode->next = listNode; 
       cout << "added" << newNode->val << endl;  
       return newNode;
      }


    static bool isPalindrome(ListNode* head) {
        
        int cnt = 0;
        ListNode* curr = head;
        ListNode* prev = NULL;
        
        while( curr != NULL) //end of list 
        {
            cnt++; 
            curr = curr->next;
        }
       
//       cout<<"Size of array is " << cnt << endl; 
        
        prev = NULL;
        curr=head;
        int mid_cnt =0;
        while(mid_cnt<= cnt/2-1)
        {
            mid_cnt++;
            prev = curr;
            curr = curr->next;
        }
       
  //      cout << "Debug::" << prev->val << ":" << curr->val <<endl; 
 
        ListNode* mid_left = prev; // mid in odd, right one in even
        ListNode* mid_right;
        if(cnt%2 !=0)
            curr = curr->next;
        mid_right = curr; // mid in odd, right one in even
            
        
        
        ListNode* tmp;
        prev = NULL;
        curr = head;
        
        //reverse direction 
        while(curr != mid_right)
        {
            tmp = curr -> next;
            curr->next = prev;
            prev = curr;
            curr = tmp;
        }
        
        //now compare 
        ListNode* curr_left = mid_left; 
        ListNode* curr_right = mid_right;
        while( curr_left != NULL)
        {
            if(curr_right->val != curr_left->val)
                return false;
            curr_right = curr_right->next;
            curr_left = curr_left->next;
        }
        
        return true;
        
    }
};



int main()
{

  ListNode l(1);
  ListNode* lp = &l;
 
  ListNode* tmp = new ListNode(2);
  lp = Solution::add(lp,tmp);


  tmp = new ListNode(1);
  lp = Solution::add(lp,tmp);

/*  ListNode l12 = l1.add(2);
  ListNode l122 = l12.add(3);
  ListNode l1221 = l122.add(1); 
*/
 cout << "List Node good" << endl;
 ListNode*  curr = lp;   
 while(curr != NULL)
  {
   cout << "List element:" << curr->val << endl;
   curr = curr->next;  
  }

 cout << "result =" << Solution::isPalindrome( lp ) << endl;

}

