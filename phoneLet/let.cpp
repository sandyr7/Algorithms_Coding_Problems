#include <string>
#include <iostream>
#include <iterator>
#include <vector>
#include <unordered_map>
using namespace std;



class Solution {
    
    
public:
static unordered_map<char, vector<string>> Digit2String; 
    vector<string> letterCombinations(string digits) {
        
        if(digits.size()==0){
           return vector<string>(1,""); 
        }
        else if(digits.size()==1){
           return Digit2String[digits[0]];
        }
        else
        {
            string digitsFrom2 = "";
            auto i = digits.begin()+1;
            for(; i!=digits.end();i++)
             digitsFrom2.push_back(*i);
            
            vector<string> tmp = letterCombinations(digitsFrom2);
            vector<string> out; 
            return out; 
            
        }
        
        
    }
    
    static unordered_map<char, vector<string>> findStringMap(){
      unordered_map<char, vector<string>> D2S;
      D2S['1'] = { "a","b","c"};  
        
       return D2S; 
    }
    
     
};

unordered_map<char, vector<string>> Solution::Digit2String = Solution::findStringMap(); 




int main(){

cout << Solution::Digit2String['1'][1]  <<endl;

}
